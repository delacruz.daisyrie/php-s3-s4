<?php 


class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}

}
$person = new Person("Daisy", "A.", "Dela Cruz");

class Developer extends Person {
	public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

$developer = new Developer("Charles", "George", "Cheng"); 

class Engineer extends Person {
	public function printName() {
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}
$engineer = new Engineer("Molly", "Grey", "Smith"); 


// Activity 4

class Building {
	
	public $name;

	public $floors;

	public $address; 

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function printName(){
		return "The name of the building is $this->name.";
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	// Activity 4
	public function getFloors() {
		return $this->floors;
	}

	public function setFloors($floors) {
		$this->floors = $floors;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


// Inheritance and Polymorphism

class Condominium extends Building {

	public function printName() {
		return "The name of the condominium is $this->name.";
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	// Activity 4
	public function getFloors() {
		return $this->floors;
	}

	public function setFloors($floors) {
		$this->floors = $floors;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}


}

// Instance
$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");












?>