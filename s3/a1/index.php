<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S3-4: Activity</title>
	</head>
	<body>

		<h1>Activity 3</h1>

		<h2>Person</h2>
		<p><?php echo($person->printName() )?></p>

		<h2>Developer</h2>
		<p><?php echo($developer->printName() )?></p>

		<h2>Engineer</h2>
		<p><?php echo($engineer->printName() )?></p>


		<h1>Activity 4</h1>
		<h2>Building</h2>

		<p>The name of the building is <?php echo $building->getName(); ?>.</p>

		<p>The Caswyn Building has <?php echo $building->getFloors(); ?> floors.</p>

		<p>The Caswyn Building is located at <?php echo $building->getAddress(); ?>.</p>

		<p><?php $building->setName("Caswyn Complex") ?></p>

		<p>The name of the building has been changed to <?php echo $building->getName() ?>.</p>



		<h2>Condominium</h2>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

		<p>The Enzo Condo has <?php echo $condominium->getFloors(); ?> floors.</p>

		<p>The Caswyn Building is located at <?php echo $condominium->getAddress(); ?>.</p>

		<p><?php $condominium->setName("Enzo Tower") ?></p>

		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>


	</body>
</html>