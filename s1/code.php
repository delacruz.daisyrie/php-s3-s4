<?php 


// Variables
// Variables are defined using the dollar notation($) before the name of the variable
$name = "John Smith";
$email = "johnsmith@gmail.com";

// Constants
define('PI', 3.1416);

// Data Types

// Strings
$state = "New York";
$country = "United States of America";
// $address = $state.", " .$country; - concatenation thru dot sign
$address = "$state, $country"; //concatenation thru double quotes

// Integers
$age = 31;
$headcount = 26;

// decimal
$grade = 98.2;
$distanceInKilometers = 1324.34;

// boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// null
$boyfriend = null;
$girlfriend = null;

// Arrays
$grades = array(90.1, 87, 98, 93);

// Objects
$gradesObj = (object) [
		'firstGrading' => 98.7,
		'secondGrading' => 92.1,
		'thirdGrading' => 90.2,
		'fourthGrading' => 95
];

$personObj = (object) [
	'fullName' => 'John Doe',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'United STates of America'
	]
];

// Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// Functions
function getFullName($firstName = "User" , $middleInitial = "User", $lastName = "User") {
	return "$lastName, $firstName, $middleInitial";
}

// Selection Control Structures
// If-ElseIf-Else Statement
function determineTyphoonIntensity ($windSpeed) {
	if ($windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61) {
		return 'Tropical depression detected';
	} else if ($windSpeed >= 62 && $windSpeed <=88) {
		return 'Tropical storm detected';
	} else if ( $windSpeed >= 89 && $windSpeed <= 117) {
		return 'Severe tropical storm detected';
	} else {
		return 'Typhoon detected';
	}
};

// Conditional (Ternary) Operator

function isUnderAge($age) {
	return ($age < 18) ? true : false;
}

// SWITCH STATEMENTS

function determineComputerUser($computerNumber) {
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sids Meier';
			break;
		case 4: 
			return 'Onel de Guzman';
			break;
		case 5: 
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber.' is out of bounds.';
			break;
	}
}


// Try-Catch-Finally
function greeting($str) {
	try {
		// Attempt to execute a code
		if(gettype($str) == "string"){
			echo $str;
		} else {
			throw new Exception("Ooooopss!");
		}
	}
	catch (Exception $e) {
		// Catch the errors within the "try"
		echo $e-> getMessage();
	}
	finally {
		// continue execution of code regardless of success and failuren of code execution
		echo "I did it again!";
	}
}



// Activity1

function getFullAddress($country, $city, $province, $specificAdd) {
	return "$specificAdd, $city, $province, $country";
}

// Activity2
function getLetterGrade($grade) {
	if ($grade < 75 ){
		return "$grade is equivalent to F";
	} else if ($grade >= 75 && $grade <=76){
		return "$grade is equivalent to C-";
	} else if ($grade >= 77 && $grade <=79){
		return "$grade is equivalent to C";
	} else if ($grade >= 80 && $grade <=82){
		return "$grade is equivalent to C+";
	} else if ($grade >= 83 && $grade <=85){
		return "$grade is equivalent to B-";
	} else if ($grade >= 86 && $grade <=88){
		return "$grade is equivalent to B";
	} else if ($grade >= 89 && $grade <=91){
		return "$grade is equivalent to B+";
	} else if ($grade >= 92 && $grade <=94){
		return "$grade is equivalent to A-";
	}  else if ($grade >= 95 && $grade <=97){
		return "$grade is equivalent to A";
	} else if ($grade >= 98 && $grade <=100){
		return "$grade is equivalent to A+";
	} 

}




?>